<?php

/*
 * This file is part of the FluidTYPO3/FluidcontentCore project under GPLv2 or later.
 *
 * For the full copyright and license information, please read the
 * LICENSE.md file that was distributed with this source code.
 */

/**
 * Updater Script for fluidcontent_core
 *
 * @package FluidcontentCore
 */
// @codingStandardsIgnoreStart
class ext_update33bbf6a313c099a7bc5582acd8034c02 extends FluidTYPO3\FluidcontentCore\Service\UpdateService {

}
